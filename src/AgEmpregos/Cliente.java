/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgEmpregos;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

/**
 * Classe do cliente1, inserção de curriculum, busca de vagas, registro de interesse
 * Menu próprio para o Cliente1
 * @author Giovanni e Steven
 */
public class Cliente { //CLIENTE 1 - Cliente que possui um curriculo e quer uma vaga

    /**
     * Função main para execução do lado do cliente1
     * @param args the command line arguments
     * @throws java.rmi.RemoteException
     * @throws java.rmi.NotBoundException
     */
    public static void main(String[] args) throws RemoteException, NotBoundException {
        // TODO code application logic here
        Registry registro = LocateRegistry.getRegistry();
        CliImpl cliente = new CliImpl((InterfaceServ)registro.lookup("Servidor"));
        
        //variaveis
        String resposta;            //variável que armazena a resposta dada no menu
        String subresposta;         //variável que armazena a resposta dada na consulta para escolher quais parâmetros serão utilizados na pesquisa
        
        String nome;                //variavel para armazenar o nome que sera utilizado no curriculo
        String contato;             //variavel para armazenar o contato para o curriculo
        String areaDeInteresse;     //variavel para guardar a area de interesse do candidato para possiveis vagas
        Integer cargaHoraria;       //variavel para armazenar a carga horaria pretendida para as vagas
        Integer salarioPretendido;  /*variavel que armazena o salario pretendido do candidato para ser 
                                    utilizada durante a criação do cadastro e na consulta como salario minimo pretendido*/
        
        String menu = "O que gostaria de fazer?\n 1 - Cadastro\n 2 - Consultar Vagas\n 3 - Registrar Area de Interesse";//texto do menu principal
        
        Scanner scanner = new Scanner(System.in);//scanner para entrada de dados
        
        //Inicio de menu
        while(true){//loop infinito para a aplicação continuar rodando até ser encerrada
            System.out.println(menu);
            resposta = scanner.next();
            switch(resposta){
                case "1"://Cadastro
                    System.out.println("Por favor, preencha os campos do cadastro:");
                    System.out.print("Nome: ");
                    nome = scanner.next();
                    System.out.print("Contato: ");
                    contato = scanner.next();
                    System.out.print("Area de Interesse: ");
                    areaDeInteresse = scanner.next();
                    System.out.print("Carga Horaria(Apenas Numero): ");
                    cargaHoraria = scanner.nextInt();
                    System.out.print("Salario Pretendido(Apenas Numero): ");
                    salarioPretendido = scanner.nextInt();
                    cliente.servidor.CadastroCurriculo(nome, contato, areaDeInteresse, cargaHoraria, salarioPretendido,cliente);
                    menu = "O que gostaria de fazer?\n 1 - Alterar Cadastro\n 2 - Consultar Vagas\n 3 - Registrar Area de Interesse";
                    break;
                    
                case "2"://Consultar Vagas
                    System.out.print("Deseja consultar por qual parâmetro?\n1-Area de Interesse\n2-Salario Minimo\n3-Area de Interesse e Salario Minimo");
                    subresposta = scanner.next();
                    
                    //segundo switch para alternar os parâmetros utilizados na pesquisa/consulta
                    switch(subresposta){
                        case("1"):
                            System.out.print("Area de Interesse: ");
                            areaDeInteresse = scanner.next();
                            cliente.servidor.ConsultaVaga(areaDeInteresse, cliente);
                            break;
                        case("2"):
                            System.out.print("Salario Mínimo: ");
                            salarioPretendido = scanner.nextInt();
                            cliente.servidor.ConsultaVaga(salarioPretendido, cliente);
                            break;
                        case("3"):
                            System.out.print("Area de Interesse: ");
                            areaDeInteresse = scanner.next();
                            System.out.print("Salario Mínimo: ");
                            salarioPretendido = scanner.nextInt();
                            cliente.servidor.ConsultaVaga(areaDeInteresse, salarioPretendido, cliente);
                            break;
                            
                        //default utilizado caso nenhum número válido seja escolhido para a subresposta
                        default:
                            System.out.println("Opção não-válida!");
                            break;
                    }
                    break;
                case "3"://Registrar Area de Interesse
                    System.out.println("Qual area você tem interesse em receber notificações?");
                    areaDeInteresse = scanner.next();
                    cliente.servidor.RegistroInteresseVaga(areaDeInteresse, cliente);
                    break;
                default://caso o usuário utilize outro número ou outro caracter que não leve a nenhuma das opções anteriores
                    System.out.println("Escolha um número válido!");
            }
        }
    }
    
}
