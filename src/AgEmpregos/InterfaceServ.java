/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgEmpregos;

import java.rmi.*;

/**
 * Interface do servidor, onde chama todos os métodos necessários para gerência
 * Consultas de vagas, consulta de curriculum
 * Registro de vagas, registro de curriculum
 * Registro de interesse de vaga e curriculum
 * @author Giovanni e Steven
 */
public interface InterfaceServ extends Remote{
    //Chamadas do Cliente1, que busca emprego
    public void ConsultaVaga(String areaDeInteresse, InterfaceCli1 cliente) throws RemoteException;
    public void ConsultaVaga(Integer salarioPretendido, InterfaceCli1 cliente) throws RemoteException;
    public void ConsultaVaga(String areaDeInteresse, Integer salarioPretendido, InterfaceCli1 cliente) throws RemoteException;
    public void CadastroCurriculo(String nome,String contato,String areaDeInteresse, Integer cargaHoraria, Integer salarioPretendido, InterfaceCli1 clienteRef) throws RemoteException;
    
    //Chamada de Registro de interesse de vaga pelo Cliente1
    public void RegistroInteresseVaga(String areaDeInteresse, InterfaceCli1 cliente) throws RemoteException;

}
