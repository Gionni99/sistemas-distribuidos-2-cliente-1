/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgEmpregos;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Classe de implementação do Cliente que busca vagas (servente)
 * @author Giovanni e Steven
 */
public class CliImpl extends UnicastRemoteObject implements InterfaceCli1{

    InterfaceServ servidor;
    
    /**
     * Construtor
     * @param servidor
     * @throws RemoteException 
     */
    
    public CliImpl(InterfaceServ servidor) throws RemoteException {
        this.servidor=servidor;
    }
    /**
     * Método de chamada, imprime String 'qualquer'
     * @param qualquer
     * @throws RemoteException 
     */
    
    @Override
    public void Chamada(String qualquer) throws RemoteException {
        System.out.println(qualquer);
    }
    
}
