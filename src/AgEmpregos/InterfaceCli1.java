/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AgEmpregos;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Interface do cliente 1
 * Método de chamada para imprimir a String 'qualquer'
 * @author Giovanni e Steven
 */
public interface InterfaceCli1 extends Remote{
    public void Chamada(String qualquer) throws RemoteException;
}
